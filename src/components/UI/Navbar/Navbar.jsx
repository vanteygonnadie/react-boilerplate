import React from 'react'
import { Link } from "react-router-dom";


const Navbar = () => {
  return (
    <nav className='navbar'>
    <Link className='navbar__link' to="/">Home</Link>
    <Link className='navbar__link' to="/about">About me</Link>
    <Link className='navbar__link' to="/content">Content</Link>
  </nav>
  )
}

export default Navbar