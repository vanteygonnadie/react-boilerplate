import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import { BrowserRouter, Route, Routes } from "react-router-dom";
import Content from './pages/Content';
import About from './pages/About';

ReactDOM.render(
  <BrowserRouter>
    <Routes>
        <Route path="/" element={<App/>}/>      
        <Route path="/about" element={<About/>}/>      
        <Route path="/content" element={<Content/>}/>
        <Route path="*" element={<h1>404!!!</h1>}/>
    </Routes>
  </BrowserRouter>,
  document.getElementById('root')
);

