import React from 'react'
import Counter from '../components/Counter'
import Navbar from '../components/UI/Navbar/Navbar'

const Content = () => {
  return (
    <div>
        <Navbar/>
        <Counter/>
    </div>
  )
}

export default Content